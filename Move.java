/**
 * Game move.
 * 
 * @author Kim Korsching
 */
public class Move
{
    public char moveType;
    public int column;
    public int row;
    public int evaluation;
    public int killerCount;

    public Move(char pMoveType, int pColumn){
        this.moveType = pMoveType;
        this.column = pColumn;
        this.evaluation = 0;
        this.killerCount = 0;
    }

    public Move(char pMoveType, int pColumn, int pEvaluation, int pKillerCount){
        this.moveType = pMoveType;
        this.column = pColumn;
        this.evaluation = pEvaluation;
        this.killerCount = pKillerCount;
    }
}
