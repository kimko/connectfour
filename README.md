# README #

A text-based variant of Connect Four I developed for the course AI for Games and Puzzles at the University College Dublin, Ireland. The game features a computer opponent using the NegaMax algorithm with Alpha-Beta-Pruning and a Killer Heuristic, with variable starting player and search depth.

The novelty of my variant is that a player may choose not to drop a disc, but instead forbid their opponent to drop a disc into a particular column on their move (veto move). There need to be at least two normal moves between two veto moves.

Written in Java
Winter Semester 2014 