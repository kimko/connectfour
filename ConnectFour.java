import java.util.*;
/**
 * Connect-4 variant with Killer Heuristic.
 * 
 * A game is started with the method startGame(). It can be decided who starts, if the computer 
 * plays with or without killer heuristic in his search, to what depth the search goes and if 
 * search information on every searched node is to be printed out. 
 * 
 * Games against the computer are played via the makeInput() method. 
 * Games against another person or yourself are played directly via the makeMove() method.
 * 
 * @author Kim Korsching 
 */
public class ConnectFour
{
    //array representing the gameboard
    int[][] board;
    //depths to which the negamax alphabeta algorithm searches
    int searchDepth;
    //number of static evaluations performed
    int staticEvaluations;
    //true if player 1 is at turn
    boolean myTurn;
    //true if the computer is using the negamax alphabeta search WITH the killer heuristic
    boolean killerHeuristicOn;
    //true if information on every evaluated node is to be printed on the console
    boolean printSearchInformation;
    //list of past moves
    LinkedList<Move> pastMoves;
    //array of Killer Moves for each depth
    Move[] killerMoves;
    //array of all possible moves (14) to save cutoff counts in (for killer heuristic)
    ArrayList<Move> possibleMoves;
    //best move determined by the alphabeta search
    Move bestMove;

    /**
     * Starts new game with a 7x5 board.
     * 
     * @param pMyTurn true if player 1 starts, false if player 2 starts
     * @param pKillerHeuristicOn true if Killer Heuristic is to be used
     * @param pSearchDepth depth of the negamax alphabeta search
     * @param pPrintSearchInformation true if information on every evaluated node is to be printed 
     */
    public void startGame(boolean pMyTurn, boolean pKillerHeuristicOn, 
    int pSearchDepth, boolean pPrintSearchInformation){
        //initialize global variables
        board = new int[7][5];
        myTurn = pMyTurn;
        killerHeuristicOn = pKillerHeuristicOn;
        searchDepth = pSearchDepth;
        printSearchInformation = pPrintSearchInformation;
        killerMoves = new Move[searchDepth];
        pastMoves = new LinkedList<Move>();
        possibleMoves = generatePossibleMoves();
        staticEvaluations = 0;
        //print out information
        System.out.print("New Game. Player 1 = X, Player 2 = 0. Player ");
        if(myTurn) {
            System.out.print(1);
        } else{
            System.out.print(2);
        }
        System.out.print(" starts. Search depth: " + searchDepth + ". Playing ");
        if(killerHeuristicOn) {
            System.out.print("with ");
        }
        else {
            System.out.print("without ");
        }
        System.out.println("Killer Heuristic.");
        print();
        if(!myTurn){
            //player 2 turn
            computerMove();
        }
    }

    /**
     * Input function for the player.
     * 
     * @param player 1 for player (p1), 2 for computer (p2)
     * @param input string declaring the type of move (d, drop or v, veto) and column (1-7)
     */
    public void makeInput(int player, String input){
        if(result(false) == 0){
            //player 1 turn
            char moveType = input.charAt(0);
            int column = Character.getNumericValue(input.charAt(1)) - 1;
            Move move = new Move(moveType, column);
            makeMove(player, move, true);
            if(result(false) == 0){
                //player 2 turn
                computerMove();
            }
        }
        else {
            System.out.println("Game already finished and was won by player " + result(false));
        }
    }

    /**
     * Performs move.
     * 
     * @param player which player (1 for player, 2 for computer)
     * @param move the move being performed
     * @param print true if information & state of the board is to be printed
     */
    public void makeMove(int player, Move move, boolean print){
        if(isPlaying(player) == true){
            if(isLegal(move)){
                if(move.moveType == 'd'){
                    move.row = drop(player, move.column, print);
                    pastMoves.add(move);
                    playerChange();
                    result(print);
                }
                else if(move.moveType == 'v') {
                    if(print) {
                        System.out.println("Veto on column " + (move.column+1) + " by player " + player);
                    }
                    pastMoves.add(move);
                    playerChange();
                    result(print);
                }
                else{
                    System.out.println("Not a valid movetype, " + player);
                }
            }
            else{
                System.out.println("Not a valid move, " + player);
            }
        }
        else {
            System.out.println("Not your turn, player " + player);
        }
    }

    /**
     * Removes the last move.
     * 
     * @param move last move
     */
    public void removeMove(Move move){
        if(pastMoves.isEmpty() == false){
            if(move.moveType == 'd'){
                board[move.column][move.row] = 0;
            }
            pastMoves.removeLast();
            playerChange();
        }
    }

    /**
     * Generates an ArrayList of possible moves in the current situation.
     */
    public ArrayList<Move> generatePossibleMoves(){
        ArrayList<Move> moves = new ArrayList<Move>();
        //add drop moves
        for(int i = 0; i < board.length; i++){
            //if column not full OR vetoed
            if(board[i][0] == 0 && isVetoed(i) == false){
                moves.add(new Move('d', i));
            }
        }
        //add veto moves
        for(int i = 0; i < board.length; i++){
            //if veto is legal (no vetos in the last two moves)
            if(isVetoLegal() == true){
                moves.add(new Move('v', i));
            }
        }
        return moves;
    }

    /**
     * Checks if the passed column is vetoed.
     */
    public boolean isVetoed(int column){
        if(pastMoves.isEmpty()){
            return false;   
        }
        else{
            //did the last move veto the column?
            if(pastMoves.peekLast().moveType == 'v' && 
            pastMoves.peekLast().column == column){
                return true;
            }
            else{
                return false;
            }
        }
    }

    /**
     * Checks if a veto is currently possible.
     */
    public boolean isVetoLegal(){
        //if there are no past moves, veto is legal
        if(pastMoves.isEmpty()){
            return true;   
        }
        else {
            //if last move was not a veto move...
            if(pastMoves.peekLast().moveType == 'd'){
                //check if there are more than 1 past move, then check second last move
                if(pastMoves.size() > 1){
                    //if last & second last move are not veto moves, veto is legal
                    if(pastMoves.get(pastMoves.size()-2).moveType == 'd'){
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                //if there is only one past move, and it is not a veto move, veto is legal
                else {
                    return true;
                }
            }
            else{
                return false;
            }
        }
    }

    /**
     * Checks if a move is legal by comparing it to the list of possible moves.
     */
    public boolean isLegal(Move move){
        for(Move possibleMove : generatePossibleMoves()){
            if(possibleMove.moveType == move.moveType &&
            possibleMove.column == move.column){
                return true;
            }
        }
        return false;
    }

    /**
     * Performs a drop move.
     * 
     * @param print if true, prints the game board on the console
     */
    public int drop(int player, int column, boolean print){
        int row = 0;
        //search for correct row to place stone in
        while(board[column][row] == 0 && row < 4 && board[column][row+1] == 0){
            row++;
        }
        board[column][row] = player;
        if(print){
            print();
        }
        return row;
    }

    /**
     * Checks if it is the passed player's turn.
     */
    public boolean isPlaying(int player){
        if (player == 1 && myTurn){
            return true;
        }
        else if (player == 2 && !myTurn){
            return true;
        }
        else { 
            return false;
        }
    }

    /**
     * Changes player-to-move.
     */
    public void playerChange(){
        if (myTurn == true) { 
            myTurn = false; 
        }
        else if (myTurn == false) { 
            myTurn = true; 
        }
    }

    /**
     * Prints gameboard. 
     * '.' are empty spaces, p2 uses '0' symbols and p1 uses 'X' symbols.
     */
    public void print(){
        for(int i=0; i < board[i].length; i++){
            for(int j=0; j < board.length; j++){
                if(board[j][i] == 0){
                    System.out.print("." + " ");
                }
                else if(board[j][i] == 2){
                    System.out.print("0" + " ");
                }
                else if(board[j][i] == 1){
                    System.out.print("X" + " ");
                }
            }
            System.out.println("");
        }
        System.out.println("");
    }

    /**
     * Checks if a player has won (four stones in a row in any direction).
     * 
     * @return 1 if p1 won, 2 if p2 won, 0 if nobody won
     * @param print if true, prints result
     */
    public int result(boolean print){
        if (horizontal() == 1 || vertical() == 1 || diagonalRight() == 1 || diagonalLeft() == 1){
            if(print){
                System.out.println("Player 1 wins."); 
            }
            return 1;
        }
        else if (horizontal() == -1 || vertical() == -1 || diagonalRight() == -1  || diagonalLeft() == -1){
            if(print){
                System.out.println("Player 2 wins.");
            }
            return 2;
        }
        else{
            return 0;
        }
    }

    /**
     * Checks if a player has four stones in a row vertically. 
     * @return 1 if p1 won, -1 if p2 won, 0 if nobody has four in a row.
     */
    public int vertical(){
        int p1Count = 0;
        int p1Consecutive = 0;
        int p2Count = 0;
        int p2Consecutive = 0;

        for(int i=0; i < board.length; i++){
            for(int j=0; j < board[j].length; j++){
                if(board[i][j] == 0){
                    p1Consecutive = 0;
                    p2Consecutive = 0;
                }
                else if(board[i][j] == 1){
                    p1Count++;
                    p1Consecutive++;
                    if(p1Count == 4 && p1Consecutive == 4){
                        return 1;
                    }
                    p2Consecutive = 0;
                } 
                else if (board[i][j] == 2){
                    p2Count++;
                    p2Consecutive++;
                    if(p2Count == 4 && p2Consecutive == 4){
                        return -1;
                    }
                    p1Consecutive = 0;
                }
            }
            p1Count = 0;
            p1Consecutive = 0;
            p2Count = 0;
            p2Consecutive = 0;
        }
        return 0;
    }

    /**
     * Checks if a player has four stones in a row horizontally. 
     * @return 1 if p1 won, -1 if p2 won, 0 if nobody has four in a row.
     */
    public int horizontal(){
        int p1Count = 0;
        int p1Consecutive = 0;
        int p2Count = 0;
        int p2Consecutive = 0;

        for(int i=0; i < board[i].length; i++){
            for(int j=0; j < board.length; j++){
                if(board[j][i] == 0){
                    p1Consecutive = 0;
                    p2Consecutive = 0;
                }
                else if(board[j][i] == 1){
                    p1Count++;
                    p1Consecutive++;
                    if(p1Count == 4 && p1Consecutive == 4){
                        return 1;
                    }
                    p2Consecutive = 0;
                } 
                else if (board[j][i] == 2){
                    p2Count++;
                    p2Consecutive++;
                    if(p2Count == 4 && p2Consecutive == 4){
                        return -1;
                    }
                    p1Consecutive = 0;
                }
            }
            p1Count = 0;
            p1Consecutive = 0;
            p2Count = 0;
            p2Consecutive = 0;
        }
        return 0;
    }

    /**
     * Checks if a player has four stones in a row digonally (/). 
     * @return 1 if p1 won, -1 if p2 won, 0 if nobody has four in a row.
     */
    public int diagonalRight(){
        int p1Count = 0;
        int p1Consecutive = 0;
        int p2Count = 0;
        int p2Consecutive = 0;

        for(int k = 0 ; k < board.length * 2 ; k++) {
            for(int j = 0 ; j <= k ; j++) {
                int i = k - j;
                if(i < board.length && j < board[i].length) {
                    if(board[i][j] == 0){
                        p1Consecutive = 0;
                        p2Consecutive = 0;
                    }
                    else if(board[i][j] == 1){
                        p1Count++;
                        p1Consecutive++;
                        if(p1Count == 4 && p1Consecutive == 4){
                            return 1;
                        }
                        p2Consecutive = 0;
                    } 
                    else if (board[i][j] == 2){
                        p2Count++;
                        p2Consecutive++;
                        if(p2Count == 4 && p2Consecutive == 4){
                            return -1;
                        }
                        p1Consecutive = 0;
                    }
                }
            }
            p1Count = 0;
            p1Consecutive = 0;
            p2Count = 0;
            p2Consecutive = 0;
        }
        return 0;
    }

    /**
     * Checks if a player has four stones in a row digonally (\). 
     * Returns 1 if p1 won, -1 if p2 won, 0 if nobody has four in a row.
     */
    public int diagonalLeft(){
        int p1Count = 0;
        int p1Consecutive = 0;
        int p2Count = 0;
        int p2Consecutive = 0;
        for( int k = -(board.length-1) ; k < board.length ; k++ ) {
            for( int j = 0 ; j < board[0].length ; j++ ) {
                int i = k + j;
                if(i >= 0 && i < board.length) {
                    if(board[i][j] == 0){
                        p1Consecutive = 0;
                        p2Consecutive = 0;
                    }
                    else if(board[i][j] == 1){
                        p1Count++;
                        p1Consecutive++;
                        if(p1Count == 4 && p1Consecutive == 4){
                            return 1;
                        }
                        p2Consecutive = 0;
                    } 
                    else if (board[i][j] == 2){
                        p2Count++;
                        p2Consecutive++;
                        if(p2Count == 4 && p2Consecutive == 4){
                            return -1;
                        }
                        p1Consecutive = 0;
                    }
                }
            }
            p1Count = 0;
            p1Consecutive = 0;
            p2Count = 0;
            p2Consecutive = 0;
        }
        return 0;
    }

    /**
     * Returns the opponent of the passed player.
     */
    public int otherPlayer(int player){
        if(player == 1){
            return 2;
        }
        else if(player == 2){
            return 1;
        }
        return -1;
    }

    /**
     * Static Evaluation Function for the search algorithm.
     * Looks for two stones in a row with two spaces at one end or spaces at both ends (worth 2 evaluation points), 
     * and three stones in a row with spaces at either end (worth 4 evaluation points).
     */
    public int staticEvaluationFunction(int player){
        int evaluation = 0;

        if(result(false) == player){
            return 1000;
        }
        else if(result(false) == otherPlayer(player)){
            return -1000;
        }

        //check vertically if (. . X X) or (. X X X) for player or opponent
        for(int i=0; i < board.length; i++){
            for(int j=0; j < board[j].length-3; j++){
                if(board[i][j] == 0 && board[i][j+1] == 0 
                && board[i][j+2] == player && board[i][j+3] == player){
                    evaluation = evaluation + 2;
                }
                else if(board[i][j] == 0 && board[i][j+1] == player 
                && board[i][j+2] == player && board[i][j+3] == player){
                    evaluation = evaluation + 4;
                }
                else if(board[i][j] == 0 && board[i][j+1] == 0 
                && board[i][j+2] == otherPlayer(player) && board[i][j+3] == otherPlayer(player)){
                    evaluation = evaluation - 2;
                }
                else if(board[i][j] == 0 && board[i][j+1] == otherPlayer(player) 
                && board[i][j+2] == otherPlayer(player) && board[i][j+3] == otherPlayer(player)){
                    evaluation = evaluation - 4;
                }
            }
        }

        //check horizontally if (. . X X) or (X X . .) or (. X X .) or (. X X X) or (X X X .) for player or opponent
        for(int i=0; i < board[i].length; i++){
            for(int j=0; j < board.length-3; j++){
                if((board[j][i] == 0 && board[j+1][i] == 0 && board[j+2][i] == player && board[j+3][i] == player)
                || (board[j][i] == player && board[j+1][i] == player && board[j+2][i] == 0 && board[j+3][i] == 0)
                || (board[j][i] == 0 && board[j+1][i] == player && board[j+2][i] == player && board[j+3][i] == 0)){
                    evaluation = evaluation + 2;
                }
                else if((board[j][i] == 0 && board[j+1][i] == player 
                    && board[j+2][i] == player && board[j+3][i] == player)
                || (board[j][i] == player && board[j+2][i] == player 
                    && board[j+2][i] == player && board[j+3][i] == 0)){
                    evaluation = evaluation + 4;
                }
                else if((board[j][i] == 0 && board[j+1][i] == 0 && board[j+2][i] == otherPlayer(player) 
                    && board[j+3][i] == otherPlayer(player))
                || (board[j][i] == otherPlayer(player) && board[j+1][i] == otherPlayer(player) 
                    && board[j+2][i] == 0 && board[j+3][i] == 0)
                || (board[j][i] == 0 && board[j+1][i] == otherPlayer(player) 
                    && board[j+2][i] == otherPlayer(player) && board[j+3][i] == 0)){
                    evaluation = evaluation - 2;
                }
                else if((board[j][i] == 0 && board[j+1][i] == otherPlayer(player) 
                    && board[j+2][i] == otherPlayer(player) && board[j+3][i] == otherPlayer(player))
                || (board[j][i] == otherPlayer(player) && board[j+2][i] == otherPlayer(player) 
                    && board[j+2][i] == otherPlayer(player) && board[j+3][i] == 0)){
                    evaluation = evaluation - 4;
                }
            }
        }   

        //check diagonally (/) if (. . X X) or (X X . .) or (. X X .) or (. X X X) or (X X X .) for player or opponent
        for(int k = 0 ; k < board.length * 2 ; k++) {
            for(int j = 0 ; j <= k-3 ; j++) {
                int i = k - j;
                if(i < board.length && j < board[i].length-3 && k <= j+3) {
                    if((board[i][j] == 0 && board[k-j+1][j+1] == 0 
                        && board[k-j+2][j+2] == player && board[k-j+3][j+3] == player)
                    || (board[i][j] == player && board[k-j+1][j+1] == player 
                        && board[k-j+2][j+2] == 0 && board[k-j+3][j+3] == 0)
                    || (board[i][j] == 0 && board[k-j+1][j+1] == player 
                        && board[k-j+2][j+2] == player && board[k-j+3][j+3] == 0)){
                        evaluation = evaluation + 2;
                    }
                    else if((board[i][j] == 0 && board[k-j+1][j+1] == player 
                        && board[k-j+2][j+2] == player && board[k-j+3][j+3] == player)
                    || (board[i][j] == player && board[k-j+1][j+1] == player 
                        && board[k-j+2][j+2] == player && board[k-j+3][j+3] == 0)){
                        evaluation = evaluation + 4;
                    }
                    else if((board[i][j] == 0 && board[k-j+1][j+1] == 0 && board[k-j+2][j+2] == otherPlayer(player) 
                        && board[k-j+3][j+3] == otherPlayer(player))
                    || (board[i][j] == otherPlayer(player) && board[k-j+1][j+1] == otherPlayer(player) 
                        && board[k-j+2][j+2] == 0 && board[k-j+3][j+3] == 0)
                    || (board[i][j] == 0 && board[k-j+1][j+1] == otherPlayer(player) 
                        && board[k-j+2][j+2] == otherPlayer(player) && board[k-j+3][j+3] == 0)){
                        evaluation = evaluation - 2;
                    }
                    else if((board[i][j] == 0 && board[k-j+1][j+1] == otherPlayer(player) 
                        && board[k-j+2][j+2] == otherPlayer(player) 
                        && board[k-j+3][j+3] == otherPlayer(player))
                    || (board[i][j] == otherPlayer(player) && board[k-j+1][j+1] == otherPlayer(player) 
                        && board[k-j+2][j+2] == otherPlayer(player) 
                        && board[k-j+3][j+3] == 0)){
                        evaluation = evaluation - 4;
                    }
                }
            }
        }

        //check diagonally (\) if (. . X X) or (X X . .) or (. X X .) or (. X X X) or (X X X .) for player or opponent
        for( int k = -(board.length-1) ; k < board.length ; k++ ) {
            for( int j = 0 ; j < board[0].length-3 ; j++ ) {
                int i = k + j;
                if(i >= 0 && i < board.length && (k+j+3) <= 6) {
                    if((board[i][j] == 0 && board[k+j+1][j+1] == 0 
                        && board[k+j+2][j+2] == player && board[k+j+3][j+3] == player)
                    || (board[i][j] == player && board[k+j+1][j+1] == player 
                        && board[k+j+2][j+2] == 0 && board[k+j+3][j+3] == 0)
                    || (board[i][j] == 0 && board[k+j+1][j+1] == player 
                        && board[k+j+2][j+2] == player && board[k+j+3][j+3] == 0)){
                        evaluation = evaluation + 2;
                    }
                    else if((board[i][j] == 0 && board[k+j+1][j+1] == player 
                        && board[k+j+2][j+2] == player && board[k+j+3][j+3] == player)
                    || (board[i][j] == player && board[k+j+1][j+1] == player 
                        && board[k+j+2][j+2] == player && board[k+j+3][j+3] == 0)){
                        evaluation = evaluation + 4;
                    }
                    else if((board[i][j] == 0 && board[k+j+1][j+1] == 0 
                        && board[k+j+2][j+2] == otherPlayer(player) && board[k+j+3][j+3] == otherPlayer(player))
                    || (board[i][j] == otherPlayer(player) && board[k+j+1][j+1] == otherPlayer(player) 
                        && board[k+j+2][j+2] == 0 && board[k+j+3][j+3] == 0)
                    || (board[i][j] == 0 && board[k+j+1][j+1] == otherPlayer(player) 
                        && board[k+j+2][j+2] == otherPlayer(player) && board[k+j+3][j+3] == 0)){
                        evaluation = evaluation - 2;
                    }
                    else if((board[i][j] == 0 && board[k+j+1][j+1] == otherPlayer(player) 
                        && board[k+j+2][j+2] == otherPlayer(player) && board[k+j+3][j+3] == otherPlayer(player))
                    || (board[i][j] == otherPlayer(player) && board[k+j+1][j+1] == otherPlayer(player) 
                        && board[k+j+2][j+2] == otherPlayer(player) && board[k+j+3][j+3] == 0)){
                        evaluation = evaluation - 4;
                    }
                }
            }
        }

        return evaluation;
    }

    /**
     * Performs computer move.
     */
    public void computerMove(){
        if(killerHeuristicOn){
            negaMaxKiller(searchDepth, 2, -Integer.MAX_VALUE, Integer.MAX_VALUE);
            if(bestMove != null){
                makeMove(2, bestMove, true);
            }
            clearKillerMoves();
        }
        else {
            negaMax(searchDepth, 2, -Integer.MAX_VALUE, Integer.MAX_VALUE);
            if(bestMove != null){
                makeMove(2, bestMove, true);
            }
        }
    }

    /**
     * Negamax algorithm with alpha-beta pruning and killer heuristic.
     */
    public int negaMaxKiller(int depth, int player, int alpha, int beta) {
        if (depth == 0) {
            staticEvaluations++;
            if(printSearchInformation){System.out.println("Evaluating node at depth " + depth + " with alpha = " + alpha + 
                    " and beta = " + beta + ". Result: " + staticEvaluationFunction(player) + " (static)");}
            return staticEvaluationFunction(player);
        }
        Move bestMoveDepth = null;
        int temp;
        for(Move currentMove: killerHeuristic(generatePossibleMoves(), depth)){
            makeMove(player, currentMove, false);
            //evaluate through recursion
            temp = -negaMaxKiller(depth-1, otherPlayer(player), -beta, -alpha);
            removeMove(currentMove);
            //cutoff through alpha-beta pruning
            if(temp >= beta){
                //increase killercount (count of cutoffs caused) of this move in static possibleMoves list
                getKillerEquivalent(currentMove).killerCount++;
                //if move caused more cutoffs than previous killer move, replace move
                if(killerMoves[depth-1] != null){
                    if(getKillerEquivalent(currentMove).killerCount > killerMoves[depth-1].killerCount){ 
                        killerMoves[depth-1]= currentMove;
                    }
                }
                //if there is no killer move for this depth, add move
                else{
                    killerMoves[depth-1]= currentMove;
                }
                return temp;
            }
            //if move is best move so far, save move
            if (temp > alpha) {
                alpha = temp;
                bestMoveDepth = new Move(currentMove.moveType, currentMove.column, temp, currentMove.killerCount);
            }
        }
        bestMove = bestMoveDepth;
        if(printSearchInformation && bestMove != null){
            System.out.println("Evaluating node " + bestMove + " at depth " + depth + " with alpha = " + alpha + 
                " and beta = " + beta + ". Result: "  + bestMove.evaluation + " (search)");
        }
        return alpha;
    }

    /**
     * Returns equivalent move of passed move in static possibleMoves list.
     */
    public Move getKillerEquivalent(Move move){
        for(int i=0; i < possibleMoves.size(); i++){
            if(move.moveType == possibleMoves.get(i).moveType && move.column == possibleMoves.get(i).column){
                return possibleMoves.get(i);
            }
        }
        System.out.println("Move doesn't exist in List.");
        return null;
    }

    /**
     * Negamax algorithm with alpha-beta pruning.
     */
    public int negaMax(int depth, int player, int alpha, int beta) {
        if (depth == 0) {
            staticEvaluations++;
            if(printSearchInformation){System.out.println("Evaluating node at depth " + depth + " with alpha = "
                    + alpha + " and beta = " + beta + ". Result: " + staticEvaluationFunction(player) + " (static)");}
            return staticEvaluationFunction(player);
        }
        Move bestMoveDepth = null;
        int temp;
        for(Move currentMove: generatePossibleMoves()){
            makeMove(player, currentMove, false);
            //evaluate through recursion
            temp = -negaMax(depth-1, otherPlayer(player), -beta, -alpha);
            removeMove(currentMove);
            //cutoff through alpha-beta pruning
            if(temp >= beta){
                return temp;
            }
            //if move is best move so far, save move
            if (temp > alpha) {
                alpha = temp;
                bestMoveDepth = new Move(currentMove.moveType, currentMove.column, temp, 0);
            }
        }
        bestMove = bestMoveDepth;
        if(printSearchInformation && bestMove != null){
            System.out.println("Evaluating node " + bestMove + " at depth " + depth + " with alpha = " + 
                alpha + " and beta = " + beta + ". Result: " + bestMove.evaluation + " (search)");
        }
        return alpha;
    }

    /**
     * Reorders passed move list so that the current killer move is first, and returns this list.
     */
    public ArrayList<Move> killerHeuristic(ArrayList<Move> moves, int depth){
        if(!(killerMoves[depth-1] == null)){
            for (int i = 0; i < moves.size(); i++){
                //checks for killermove
                if (moves.get(i).moveType == killerMoves[depth-1].moveType 
                && moves.get(i).column == killerMoves[depth-1].column) {
                    //move killermove to the start of the list
                    Move killerMove = moves.get(i);
                    moves.remove(i);
                    moves.add(0, killerMove);
                }
            }
        }
        return moves;
    }

    /**
     * Resets killer moves for every level to null.
     */
    public void clearKillerMoves(){
        for(int i=0; i < killerMoves.length; i++){
            killerMoves[i] = null;
        }
        for(int i=0; i < possibleMoves.size(); i++){
            possibleMoves.get(i).killerCount = 0;
        }
    }
}
